#!/usr/bin/python
# -*- coding: utf-8 -*-

DOCUMENTATION = """
---
module: tango_starter

short_description: Tango Starter access

version_added: "1.1"

description:
    - "Using Ansible for start/stop/restart device server using tango starter"

options:
    starter:
        description:
            - Name of tango starter
        type: str
        default: tango/admin/$(hostname -s)
    server:
        description:
            - Name of device server
        type: str
        required: true
    state:
        description:
          - C(started)/C(stopped) are idempotent actions that will not run
            commands unless necessary.
          - C(restarted) will always restart the device.
          - Note that restart will start the device if it is not already started
        type: str
        choices: [restarted, started, stopped]
    enabled:
        description:
            - Specify if device server should be managed by Starter
        type: bool
        default: True
    level:
        description:
            - Specify the startup level of the device. Won't be modified if not specified
        type: int
        default: -1
    sleep:
        description:
            - Number of seconds to sleep between checks
        type: int
        default: 1
    timeout:
        description:
            -  Maximum number of seconds to wait for start/stop/restart device
        type: int
        default: 10

requirements:
- pytango
author:
    - KITS kits_controls@maxiv.lu.se
"""

EXAMPLES = """
    - name: Start DS
      tango_starter:
        starter: tango/admin/tangobox
        server: TangoTest/test
        state: started
        enabled: True
        level: 4
        timeout: 10

    - name: Stop DS
      tango_starter:
        starter: tango/admin/tangobox
        server: TangoTest/test
        state: stopped
        sleep: 2

    - name: Restart DS
      tango_starter:
        starter: tango/admin/tangobox
        server: TangoTest/test
        state: restarted
"""

RETURN = r'''#'''

# Starter mode
CONTROLLED = 1
UNCONTROLLED = 0

from ansible.module_utils.basic import AnsibleModule
import tango
import time
import socket


class TangoStarter:
    def __init__(self, starter, server, state, enabled, level, sleep, timeout):
        self.starter = starter
        self.server = server
        self.state = state
        self.enabled = enabled
        self.level = level
        self.sleep = sleep
        self.timeout = timeout
        self.meta = {}
        self.has_changed = False

    def _is_running(self, server):
        try:
            if server.count("/") == 1:
                server = "dserver/%s" % server

            tango.DeviceProxy(server).ping()
            return True
        except tango.DevFailed:
            return False

    def _wait_for_state(self, server, running=True):
        end = time.time() + self.timeout
        while time.time() < end:
            if running:
                if self._is_running(server):
                    return True
                else:
                    time.sleep(self.sleep)
            else:
                if not self._is_running(server):
                    return True
                else:
                    time.sleep(self.sleep)
        state = "start" if running else "stop"
        raise Exception("Failed to %s the %s server" % (state, server))

    def _handle_startup_info_change(self):
        starter = tango.DeviceProxy(self.starter)
        self.meta["startup_information"] = "Startup information for %s has changed" % self.server
        self.has_changed = True
        starter.command_inout("UpdateServersInfo")

    def _set_managed_status(self, enabled):
        db = tango.Database()
        info = db.get_server_info(self.server)
        if enabled:
            # detect if device info is going to change
            info_changed = info.mode != CONTROLLED or (info.level != self.level and self.level != -1)
            info.mode = CONTROLLED
            # if user didn't defined level, use level defined in device info
            if self.level != -1:
                info.level = self.level

            if info_changed:
                db.put_server_info(info)
                self._handle_startup_info_change()
        else:
            if info.mode != UNCONTROLLED:
                db.delete_server_info(self.server)
                self._handle_startup_info_change()

    def server_started(self):
        self._wait_for_state(server=self.starter, running=True)  # ensure that starter is working
        self._set_managed_status(self.enabled)
        if not self._is_running(self.server):
            starter = tango.DeviceProxy(self.starter)
            starter.command_inout("DevStart", self.server)
            self._wait_for_state(server=self.server, running=True)
            self.has_changed = True

        self.meta["started"] = "Device server started"

    def server_stopped(self):
        self._wait_for_state(server=self.starter, running=True)  # ensure that starter is working
        self._set_managed_status(self.enabled)
        if self._is_running(self.server):
            starter = tango.DeviceProxy(self.starter)
            starter.command_inout("DevStop", self.server)

            try:
                self._wait_for_state(server=self.server, running=False)
            except tango.DevError:
                starter.command_inout("HardKillServer", self.server)  # bigger gun for uncivil DS
                self._wait_for_state(server=self.server, running=False)

            self.has_changed = True
            time.sleep(self.timeout)

        self.meta["stopped"] = "Device server stopped"

    def server_restarted(self):
        self.server_stopped()
        self.server_started()

        self.has_changed = True
        self.meta["restarted"] = "Device server restarted"


def main():
    module_args = {
        "starter": {"default": "tango/admin/%s" % socket.gethostname(), "type": "str"},
        "server": {"required": True, "type": "str"},
        "state": {"default": "started",
                  "choices": ["started", "stopped", "restarted"],
                  "type": "str"},
        "enabled": {"default": True, "type": "bool"},
        "level": {"default": -1, "type": "int"},
        "sleep": {"default": 1, "type": "int"},
        "timeout": {"default": 10, "type": "int"},
    }
    module = AnsibleModule(argument_spec=module_args)
    tango_starter = TangoStarter(**module.params)

    state_map = {
        "started": tango_starter.server_started,
        "stopped": tango_starter.server_stopped,
        "restarted": tango_starter.server_restarted,
    }
    try:
        state_map.get(module.params['state'])()
        module.exit_json(changed=tango_starter.has_changed, meta=tango_starter.meta)
    except Exception as e:
        module.fail_json(msg="Error!", details=str(e)[:100])


if __name__ == '__main__':
    main()
