import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("molecule_group")


@pytest.fixture()
def distribution(host):
    return (host.system_info.distribution + str(host.system_info.release).split(".")[0]).lower()


def test_tango_starter_service(host):
    tango_starter = host.service("tango-starter")
    assert tango_starter.is_running
    assert tango_starter.is_enabled


def test_security_limits(host):
    limits = host.file("/etc/security/limits.conf")
    assert limits.contains("^tangosys    soft    nproc     4096")
    assert limits.contains("^tangosys    hard    nofile    10240")
    assert limits.contains("^tangosys    soft    nofile    10240")


def test_tango_starter_running_as_tango_user(host):
    proc = host.process.get(comm="Starter")
    assert proc.user == "tangosys"


def test_tango_starter_start_ds_path(host, distribution):
    if distribution == "centos6":
        return
    cmd = host.run(
        f'python3 -c \'import tango; print(tango.DeviceProxy('
        f'"tango/admin/{host.ansible.get_variables()["inventory_hostname"]}").get_property("startDsPath"))\''
    )
    assert cmd.rc == 0
    assert cmd.stdout.strip() == "{'startDsPath': ['/usr/bin', '/usr/local/bin']}"


def test_tango_devices(host, distribution):
    if distribution == "centos6":
        return
    cmd = host.run(
        'python3 -c \'import tango; print(tango.Database().get_device_property("test/Pool/01", "foo"))\''
    )
    assert cmd.rc == 0
    assert cmd.stdout.strip() == "{'foo': ['bar']}"
