# ans_maxiv_role_tango_server

Ansible role to setup a tango server basic requirements.

## Role Variables

See [defaults/main.yml](defaults/main.yml)

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ans_maxiv_role_tango_server
```

## License

GPL-3.0-or-later
